
function addTask_validation() {
    var a = document.forms["add_task"]["headline_of_task"].value;
    var b = document.forms["add_task"]["description"].value;
    var c = document.forms["add_task"]["area"].value;
    var d = document.forms["add_task"]["pincode"].value;   
    var f = document.forms["add_task"]["budget"].value;
    var g = document.forms["add_task"]["due_date"].value;
    var h = document.forms["add_task"]["category_id"].value;
    
    if (a == "") {
        alert("Headline can not be empty");
        document.forms["add_task"]["headline_of_task"].focus();
        return false;
    }
    if (b == "") {
        alert("Description cannot be empty");
        document.forms["add_task"]["description"].focus();
        return false;
    }
    if (c == "") {
        alert("Address cannot be blank ");
        document.forms["add_task"]["area"].focus();
        return false;
    }
    if (d == "") {
        alert("Pincode cannot be empty");
        document.forms["add_task"]["pincode"].focus();
        return false;
    }

    if (f == "") {
        alert("Billing Name can not be empty");
        document.forms["add_task"]["budget"].focus();
        return false;
    }
    if (g == "") {
        alert("billing last name cannot be empty");
        document.forms["add_task"]["due_date"].focus();
        return false;
    }
    if (h == "") {
        alert("Billing Address cannot be blank ");
        document.forms["add_task"]["category_id"].focus();
        return false;
    }
    
}
function fillForm() {
    var address = document.forms["address"]["old_address"].value;
    
    var array = address.split(",");
    
    document.getElementById("name").value = array[0];
    document.getElementById("last-name").value = array[1];
    document.getElementById("address").value = array[2];
    document.getElementById("pincode").value = array[3];
    document.getElementById("mobile-no").value = array[4];

}

function validate_login() {
    var x = document.getElementById("email").value;
    var y = document.getElementById("password").value;
    if (x == "") {
        alert("user name required");
        var x = document.getElementById("email").focus();
        return false;

    }
    if (y == "") {
        alert("password required");
        var y = document.getElementById("password").focus();
        return false;
    }
}

function register_validate() {
    var a = document.getElementById("first-name").value;
    var b = document.getElementById("last-name").value;
    var c = document.getElementById("email").value;
    var d = document.getElementById("mobile-no").value;
    var e = document.getElementById("address").value;
    var f = document.getElementById("state").value;
    var g = document.getElementById("city").value;
    var h = document.getElementById("password").value;
    if (a == "") {
        alert('First Name cannot be blank');
        document.getElementById("first-name").focus();
        return false;
    }
    if (b == "") {
        alert('Last Name cannot be blank');
        document.getElementById("last-name").focus();
        return false;
    }
    if (c == "") {
        alert('email cannot be blank');
        document.getElementById("email").focus();
        return false;
    }
    if (d == "") {
        alert('Number cannot be blank');
        document.getElementById("mobile-no").focus();
        return false;
    }
    if (e == "") {
        alert('Address cannot be blank');
        document.getElementById("address").focus();
        return false;
    }
    if (f == "") {
        alert('State cannot be blank');
        document.getElementById("state").focus();
        return false;
    }
    if (g == "") {
        alert('City cannot be blank');
        document.getElementById("city").focus();
        return false;
    }
    if (h == "") {
        alert('Password cannot be blank');
        document.getElementById("password").focus();
        return false;
    }
}
function cat_validation() {
   
    var a = document.getElementById("name").value;
    
     if (a == "") {
        alert("category cannot be empty");
        var x = document.getElementById("name").focus();
        return false;

    }
}

function description_validation(){
    
}

