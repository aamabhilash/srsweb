/**
.-------------------------------------.
|  Software: Francium Star            |
|  Version: 0.1  (2015-06-15)         |
|  http://subinsb.com/Francium-Star   |
'-------------------------------------'
*/

function log(m){
  console.log(m);
}

(function(window){
  window.Fr = window.Fr || {};
  
  Fr.star = function(elem, rated_callback){
    rated_callback = rated_callback || function(){};
    
    elem.onmousemove = function(e) {
      if(elem.getAttribute("data-disabled") == null){
        var coor = e.offsetX;
        if(coor < 12.1 && coor > 0){
            percent = 10;
        }else if(coor > 12.1 && coor < 24.2){
            percent = 20;
        }else if(coor > 24.2 && coor < 36.3){
            percent = 30;
        }else if(coor > 36.3 && coor < 48.4){
            percent = 40;
        }else if(coor > 48.4 && coor < 60.5){
            percent = 50;
        }else if(coor > 60.5 && coor < 72.6){
            percent = 60;
        }else if(coor > 72.6 && coor < 84.7){
            percent = 70;
        }else if(coor > 84.7 && coor < 96.8){
            percent = 80;
        }else if(coor > 96.8 && coor < 108.9){
            percent = 90;
        }else if(coor > 108.9 && coor < 121){
            percent = 100;
        }
        
        
        if(percent < 101){
          rating_decimal = ("" + (percent / 100) * 5 + "").substr(0, 3);
          if(rating_decimal.substr(-2) == ".9"){
            rating_decimal = Math.round(rating_decimal, 2);
          }
          elem.setAttribute("data-title", "Set a rating of " + rating_decimal);
          
          elem.querySelector(".Fr-star-value").style.width = percent + "%";
        }
      }
    };
    
    elem.onmouseout = function(){
      original_rating = elem.getAttribute("data-rating");      
      percent = (original_rating / 5) * 100;
      elem.querySelector(".Fr-star-value").style.width = percent + "%";
      
      elem.removeAttribute("data-disabled");
    };
    
    elem.onclick = function(){
      width = elem.querySelector(".Fr-star-value").style.width.replace("%", "");
      rating = ("" + (width/100) * 5 + "").substr(0, 3);
      
      if(rating.substr(-2) == ".9"){
        rating = Math.round(rating, 2);
      }
      
      elem.setAttribute("data-rating", rating);
      elem.setAttribute("data-disabled", 1);
      rated_callback(rating);
    };
  };
})(window);

(function($){  
  $.fn.Fr_star = function(rated_callback){
    return this.each(function(){
      Fr.star($(this).get(0), rated_callback);
    });
  };
})(jQuery);
