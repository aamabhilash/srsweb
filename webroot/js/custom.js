 $(document).ready(function () {
		$('.toggle-menu').jPushMenu({closeOnClickLink: false});

		$("#owl-demo").owlCarousel({
		    navigation: true, // Show next and prev buttons

		    slideSpeed: 300,
		    paginationSpeed: 400,
		    navigationText: [
			"<i class='fa fa-long-arrow-left'></i>",
			"<i class='fa fa-long-arrow-right'></i>"],
		    items: 1,
		    itemsDesktop: false,
		    itemsDesktopSmall: false,
		    pagination: false,
		    itemsTablet: false,
		    itemsMobile: false


		});

		$("#owl-demo3").owlCarousel({
		    autoPlay: 3000, //Set AutoPlay to 3 seconds
		    dots: false,
		    navigation: true,
		    singleItem: false,
		    items: 3,
		    itemsDesktop: [1199, 3],
		    itemsDesktopSmall: [979, 3],
		    navigationText: [
			"<i class='fa fa-angle-left'></i>",
			"<i class='fa fa-angle-right'></i>"],
		    pagination: false,
		});
		var grid = document.querySelector('#grid');
		var item = document.createElement('article');

	    });