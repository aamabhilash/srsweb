<div class="main-slider">
    <div class="banner-tab">    
	<div class="container">
	    <h1>Notes</h1>
	    <!-- Tab panes -->
	    <div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home" data-title="Home">
		    <?= $this->Form->create('notes', array('name' => 'filter', 'class' => 'form-inline', 'type'=>'get')); ?>
		    <div class="row">
			<div class="col-sm-3">
			    <div class="form-group">
				<?= $this->Form->select('class', $class ,['label' =>__('Class')],[ 'class' => 'form-control','label'=>false,'empty'=>'Select Class']); ?>
			    </div>
			</div>
			<div class="col-sm-3">
			    <div class="form-group">
				<?= $this->Form->select('sub_id',$subjects,['label' =>__('Subject')], [ 'class' => 'form-control','label'=>false,'placeholder'=>'Subject','data-validation' => 'required', 'data-validation-error-msg' => 'Enter subject']); ?>
			    </div>
			</div>
			<div class="col-sm-3">
			    <div class="form-group">
				<?= $this->Form->select('chapter_id' ,$chapters,['label' =>__('Chapter')],['data-validation' => 'required', 'data-validation-error-msg' => 'Select Chapter']); ?>
			    </div>
			</div>
			<div class="col-sm-3">
				<?= $this->Form->button('Submit', ['class' => 'btn btn-primary']); ?>
			    </div>
		    </div>
		    <?= $this->Form->end() ?>	  
		</div>
	    </div>

	</div>
    </div>

    <div id="owl-demo" class="owl-carousel owl-theme">       
	<div class="item">
	    <img   src="<?= $this->request->base ?>/img/rajkot-mahila-college-1.jpg" alt="The Last of us">
	</div>
    </div>
</div>
<?php if(!empty($notes)) { ?>
<div class="banner-half">
     <div class="container">       
       <h4><?php echo $notes->title ?></h4>
     </div>
   </div>
<section id="main-container"> 
     
      <div class="container">
      <div class="term-condition">
      <?php echo  $notes->notes_eng ?>
      
 </div>
      </div>
    </section>

<?php  }else{ ?>

<?php }?>