
<style>
    .form-details{
	max-width: 800px;
	margin:10px auto 30px ;
	width:100%;
	display:block;
    }

    .form-des-row{
	border:1px solid #ccc;
	padding:5px 10px;
    }
    
    .form-des-row p{
	margin-bottom: 0px;
	    
    }

    .form-des-row p strong{
	width: 37%;
	display: inline-block;
	vertical-align: top;
    }

    .form-des-row p span{
	width:60%;
	display:inline-block;
    }

    .form-des-row p span img{
	max-width:60%;
    }
</style>

<?php  foreach($teacherdetail as $data){?>
<div style=" float: right;
    margin-right: 10px;">
<a class="btn btn-info" title="<?= __('Back') ?>" href="<?= $this->Url->build(['controller'=>'Teacher','action'=>'index']) ?>" ><i class="entypo-pencil"></i> <?= __('Back') ?></a>
</div>
<div class="form-details">
          <div class="col-sm-12 form-des-row">
           <div class="row">
           <div class="col-sm-6">
            <p><strong>Name:</strong> &nbsp; <span><?= $data['name'];?></span></p>
            </div>
            <div class="col-sm-6">
            <p><strong>Mobile Number:</strong> &nbsp; <span><?= $data['mobile_no'];?></span></p>
            </div>
            </div>
          </div>
          
          <div class="col-sm-12 form-des-row">
           <div class="row">
           <div class="col-sm-6">
            <p><strong>Age:</strong> &nbsp; <span><?= $data['age'];?></span></p>
            </div>
            <div class="col-sm-6">
            <p><strong>Gender:</strong> &nbsp; <span><?php if($data['gender'] =='0'){echo 'Male';}elseif($data['gender'] =='1'){echo 'Female';}else{echo 'Data Not Found';} ;?></span></p>
            </div>
            </div>
          </div>
          
           <div class="col-sm-12 form-des-row">
           <div class="row">
           <div class="col-sm-6">
            <p><strong>Experience:</strong> &nbsp; <span><?= $data['experience'];?></span></p>
            </div>
            <div class="col-sm-6">
            <p><strong>Qualification:</strong> &nbsp;<span> <?= $data['qualification'];?></span></p>
            </div>
            </div>
          </div>
     <div class="col-sm-12 form-des-row">
           <div class="row">
           <div class="col-sm-6">
            <p><strong>Subject:</strong> &nbsp; <span><?= $data['address'];?></span></p>
            </div>
            <div class="col-sm-6">
            <p><strong>Preferred Area:</strong> &nbsp;<span> <?= $data['pincode'];?></span></p>
            </div>
            </div>
          </div>
    <div class="col-sm-12 form-des-row">
           <div class="row">
           <div class="col-sm-6">
            <p><strong>Address:</strong> &nbsp; <span><?= $data['address'];?></span></p>
            </div>
            <div class="col-sm-6">
            <p><strong>Pin-code:</strong> &nbsp;<span> <?= $data['pincode'];?></span></p>
            </div>
            </div>
          </div>
          
           <div class="col-sm-12 form-des-row">
            <p><strong>Adhar Front Side:</strong> &nbsp; <span><img src="<?= $this->request->base.'/Identity_image/front_side/'.$data['front_side_aadhar_card']; ?>" class="img-responsive"></span></p>
          </div>
          
           <div class="col-sm-12 form-des-row">
            <p><strong>Adhar Back Side:</strong> &nbsp; <span><img src="<?= $this->request->base.'/Identity_image/back_side/'.$data['back_side_aadhar_card']; ?>" class="img-responsive"></span></p>
          </div>
          
          <div class="clearfix"></div>
          
        </div>
<?php }?>