
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
	<legend> Teacher List</legend>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th  style='width:15%;'><?= __('Name') ?></th>
                    <th style='width:10%;'><?= __('Mobile No.') ?></th>
                    <th style='width:5%;'><?= __('Age') ?></th>
		    <th style='width:5%;'><?= __('Gender') ?></th>
		    <th style='width:20%;'><?= __('Experience') ?></th>
                    <th style='width:20%;'><?= __('Intrested Subject') ?></th>  
		    <th style='width:10%;'><?= __('Intrested Class') ?></th>
		    <th style='width:10%;'><?= __('Action') ?></th>
                </tr>
            </thead>
            <tbody>

		<?php
		if (isset($teacher)) {
		    foreach ($teacher as $data) {
			?>
			<tr class="gradeU">
			    <td><?= $data->name ?></td>
			    <td><?= $data->mobile_no ?></td>
			    <td><?= $data->age ?></td>
			    <td><?php if($data->tutor_preferred === '0'){ echo 'Male';}
				elseif($data->tutor_preferred ==='1'){
				    echo 'Female';
				}elseif($data->tutor_preferred ==='3'){
				    echo 'Any';
				}else{
				    echo 'No Preference';
				}
				
			    ?></td>
			    <td><?= $data->experience ?></td>
			    <td><?= $data->intrested_subject ?></td>
			    <td><?= $data->intrested_class ?></td>
			   <td class="center">   
                            <a class="btn btn-info" title="<?= __('Details') ?>" href="<?= $this->Url->build(['controller'=>'Teacher','action'=>'detail',$data->id]) ?>" ><i class="entypo-pencil"></i> <?= __('Details') ?></a>
                        </td>
			</tr>
		    <?php
		    }
		} else {

		    echo '<tr>No Data Found</tr>';
		}
		?>    
            <tbody>
        </table>
    </div>
</div>