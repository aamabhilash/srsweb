<div class="box">
    <!-- /.box-header -->
    <div class="box-body">          
        <div class=" nav navbar-form" style="float: right">
            <?php echo $this->Form->create('search', array('type' => 'get')); ?>
            <div class="search">
                <?php
                echo $this->Form->input('sub_name', array('type' => 'search', 'placeholder' => (!empty($this->request->query('sub_name'))) ? $this->request->query('sub_name') : 'subject name', 'label' => FALSE, 'templates' => ['inputContainer' => '{{content}}'], 'class' => 'form-control'));
                echo $this->Form->button($this->Html->image('icon_search.png', ['alt' => '']), array('class' => 'btn btn-primary'));
                ?>                               
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <table id="example1" class="table table-striped">
            <thead>
                <tr>
                    <td colspan="2" style="margin-bottom: 10px; text-align: center; font-weight: bold; font-size: 30px;"><?= __('Subjects') ?></td>
                    <td><a class="btn btn-info" href="<?= $this->Url->build(["controller" => "Subjects", "action" => "add"]); ?>" style="float: right;margin-bottom: 10px;">Add</a></td>
                </tr>
                <tr>
                    <th><?= __('ID') ?></th>
                    <th><?= __('Name') ?></th>                   
                    <th><?= __('Action') ?></th>                    
                </tr>
            </thead>
            <tbody>
                <?php foreach ($subjects as $sub) {
                    ?>
                    <tr class="gradeU">
                        <td><?= $sub->sub_id ?></td>
                        <td><?= $sub->title ?></td>
                        <td class="center"> 
                            <a class="btn btn-info" title="<?= __('Edit') ?>" href="<?= $this->Url->build(['controller' => 'Subjects', 'action' => 'edit', $sub->sub_id]) ?>" >Edit</a>
                            <a href="<?= $this->Url->build(['controller' => 'Subjects', 'action' => 'delete', $sub->sub_id, $this->request->getParam('_csrfToken')]) ?>" class="btn btn-danger" title="<?= __('Delete subject') ?>" onclick="if (confirm('Do you really want to delete this subject?')) {
                                            return true;
                                        }
                                        return false;">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            <tbody>
        </table>
        <div class="pagination" style="float: right">
            <?php echo $this->Paginator->prev('' . ('<')) ?>
            <?php echo $this->Paginator->numbers(); ?>
            <?php echo $this->Paginator->next('' . ('>')) ?>
        </div>
    </div>    
</div>
