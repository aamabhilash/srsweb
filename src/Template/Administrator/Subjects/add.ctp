<div class="container" style="min-height: 80%">
    <div class="col-sm-10">
        <fieldset>
            <legend>Add Subject</legend>
            <div class="form-group">
                <?= $this->Form->create('subject',array('name' => 'add')); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('title', ['class' => 'form-control']) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->button('Add', ['class' => 'btn btn-primary']); ?>
            </div>
        </fieldset>
    </div>
</div>
