

<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
	<legend>Student List</legend>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th  style='width:10%;'><?= __('Name') ?></th>
		     <th style='width:15%;'><?= __("Parent's Name") ?></th>
                    <th style='width:10%;'><?= __('Mobile No.') ?></th>
                    <th style='width:10%;'><?= __('phone_no') ?></th>
		    <th style='width:5%;'><?= __('class') ?></th>
		    <th style='width:10%;'><?= __('subject') ?></th>
                    <th style='width:15%;'><?= __('address') ?></th>
                    <th style='width:15%;'><?= __('Tutor Preferred') ?></th> 
		    <th style='width:10%;'><?= __('Action') ?></th>
		    
                </tr>
            </thead>
            <tbody>

		<?php
		if (isset($student)) {
		    foreach ($student as $data) {
			?>
			<tr class="gradeU">
			    <td><?= $data->name ?></td>
			    <td><?= $data->parent_name ?></td>
			    <td><?= $data->mobile_no ?></td>
			    <td><?= $data->phone_no ?></td>
			    <td><?= $data->class ?></td>
			    <td><?= $data->subject ?></td>
			    <td><?= $data->address ?></td>
			    
			    <td><?php if($data->tutor_preferred === '0'){ echo 'Male';}
				elseif($data->tutor_preferred ==='1'){
				    echo 'Female';
				}elseif($data->tutor_preferred ==='3'){
				    echo 'Any';
				}else{
				    echo 'No Preference';
				}
				
			    ?></td>
			   <td class="center">   
                            <a class="btn btn-info" title="<?= __('Details') ?>" href="<?= $this->Url->build(['controller'=>'Student','action'=>'detail',$data->id]) ?>" ><i class="entypo-pencil"></i> <?= __('Details') ?></a>
                        </td>
			</tr>
		    <?php
		    }
		} else {

		    echo '<tr>No Data Found</tr>';
		}
		?>    
            <tbody>
        </table>
    </div>
</div>