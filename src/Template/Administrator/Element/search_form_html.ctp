<?php
if (!isset($is_date_filter)) {
    $is_date_filter = false;
}
?>
<div id="example1_filter" class="dataTables_filter">
<?= $this->Form->create('Search', ['type' => 'get']); ?>
    <label>Search:<input class="form-control input-sm" placeholder="" aria-controls="example1" type="search"></label>
<?= $this->Form->end() ?>
</div>

