<ul id="main-menu" class="sidebar-menu tree" data-widget="tree">
    <li <?php echo (($this->request->params['controller'] == 'Users') && ($this->request->params['action'] == 'dashboard')) ? 'class="active"' : ''; ?>>
	<?php echo $this->Html->link('<i class="entypo-gauge"></i><span class="title">' . __('Dashboard') . '</span>', ['controller' => 'Users', 'action' => 'dashboard'], array('escape' => false, 'class' => 'treeview', "tabindex" => -1)); ?>
    </li>
    <li <?php echo (($this->request->params['controller'] == 'Teacher') && ($this->request->params['action'] == 'index')) ? 'class="active"' : ''; ?>>

	<?php echo $this->Html->link('<i class="entypo-gauge"></i><span class="title">' . __('Teachers') . '</span>', ['controller' => 'Teacher', 'action' => 'index'], array('escape' => false, 'class' => 'treeview', "tabindex" => -1)); ?>
    </li>
    <li<?php echo (($this->request->params['controller'] == 'Subjects') && ($this->request->params['action'] == 'index')) ? 'class="active"' : ''; ?>>
	<?php echo $this->Html->link('<i class="entypo-gauge"></i><span class="title">' . __('Subjects') . '</span>', ['controller' => 'Subjects', 'action' => 'index'], array('escape' => false, 'class' => 'treeview', "tabindex" => -1)); ?>
    </li>
    <li<?php echo (($this->request->params['controller'] == 'Notes') && ($this->request->params['action'] == 'index')) ? 'class="active"' : ''; ?>>
	<?php echo $this->Html->link('<i class="entypo-gauge"></i><span class="title">' . __('Notes') . '</span>', ['controller' => 'Notes', 'action' => 'index'], array('escape' => false, 'class' => 'treeview', "tabindex" => -1)); ?>
    </li>
        <!-- Student list -->
    <li<?php echo (($this->request->params['controller'] == 'Student') && ($this->request->params['action'] == 'index')) ? 'class="active"' : ''; ?>>
	<?php echo $this->Html->link('<i class="entypo-gauge"></i><span class="title">' . __('Students') . '</span>', ['controller' => 'Student', 'action' => 'index'], array('escape' => false, 'class' => 'treeview', "tabindex" => -1)); ?>
    </li>

    <li class="treeview">
	<a href="#">
            <i class="fa fa-telegram"></i>
            <span>Testimonial</span>
            <span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
            </span>
	</a>
	<ul class="treeview-menu">
	    <li <?php echo (($this->request->params['controller'] == 'Testimonial') && ($this->request->params['action'] == 'add')) ? 'class="active"' : ''; ?>>
		<?php echo $this->Html->link('<i class="entypo-gauge"></i><span class="title">' . __('Add Testimonial') . '</span>', ['controller' => 'Testimonial', 'action' => 'add'], array('escape' => false, 'class' => 'treeview', "tabindex" => -1)); ?>
	    </li>
	    <li <?php echo (($this->request->params['controller'] == 'Testimonial') && ($this->request->params['action'] == 'index')) ? 'class="active"' : ''; ?>>
		<?php echo $this->Html->link('<i class="entypo-gauge"></i><span class="title">' . __('Manage Testimonials') . '</span>', ['controller' => 'Testimonial', 'action' => 'index'], array('escape' => false, 'class' => 'treeview', "tabindex" => -1)); ?>
	    </li>
	</ul>
    </li>

</ul>
