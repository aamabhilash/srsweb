<div class="row">
    <div class="col-md-6 col-sm-8 clearfix">
        <ul class="user-info pull-left pull-none-xsm">
            <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="text-transform: capitalize;">
                    <img src="<?= $this->request->base ?>/img/Adm/images/thumb-1@2x.png" alt="" class="img-circle" width="44" />
                    <?php $Auth->user('email') ?>
                </a>
                <ul class="dropdown-menu">
                    <li class="caret"></li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'changePassword']) ?>">
                            <i class="entypo-user"></i>
                            <?= __('Edit Profile'); ?>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-4 clearfix hidden-xs">
        <ul class="list-inline links-list pull-right">
            
            <li class="sep"></li>
            <li>
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'logout']) ?>">
                    Log Out <i class="entypo-logout right"></i>
                </a>
            </li>
        </ul>

    </div>

</div>

<!--<nav class="navbar navbar-static-top">
		    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"></a>
		    <div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
			   
			    <li class="dropdown messages-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"></a>
			    </li>

			    <li class="dropdown user user-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				    <img src="../img/avatar3.png" class="user-image" alt="">
				    <span class="hidden-xs"> <?php //echo $_SESSION['user_admin']['username'] ?></span>
				</a>
				<ul class="dropdown-menu">
				  
				    <div class="pull-left">
					<a href="<?php //echo'change_password.php'; ?>" class="btn btn-primary btn-flat"><?PHP// $LangConstant->__('changePassword'); ?></a>
				    </div>
				    <div class="pull-right">
					<a href="<?php //echo HTTP_ADMIN . '' . PAGE_ADMIN_HOME . '?logout_user=true&msg=true' ?>" class="btn btn-primary btn-flat" style="padding-right: 5px;"><?php //echo $LangConstant->__('signOut'); ?></a>
				    </div>
				   

				</ul>
			    </li>

			</ul>
		    </div>   
		</nav>
		-->