<div class="box">
    <!-- /.box-header -->
    <div class="box-body">          
        <div class=" nav navbar-form" style="float: right">
            <?php echo $this->Form->create('search', array('type' => 'get')); ?>
            <div class="search">
                <?php
                echo $this->Form->input('chapter_name', array('type' => 'search', 'placeholder' => (!empty($this->request->query('chapter_name'))) ? $this->request->query('chapter_name') : 'chapter name', 'label' => FALSE, 'templates' => ['inputContainer' => '{{content}}'], 'class' => 'form-control'));
                echo $this->Form->button($this->Html->image('icon_search.png', ['alt' => '']), array('class' => 'btn btn-primary'));
                ?>                               
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <table id="example1" class="table table-striped">
            <thead>
                <tr>
                    <td colspan="" style="margin-bottom: 10px; text-align: center; font-weight: bold; font-size: 30px;"><?= __('Notes') ?></td>
                    <td><a class="btn btn-info" href="<?= $this->Url->build(["controller" => "Notes", "action" => "add"]); ?>" style="float: right;margin-bottom: 10px;">Add</a></td>
                </tr>
                <tr>
                    <th><?= __('ID') ?></th>
                    <th><?= __('Chapter Number') ?></th>                   
                    <th><?= __('Chapter Name') ?></th>                   
                    <th><?= __('Notes English') ?></th>                   
                    <th><?= __('Notes Hindi') ?></th>                   
                    <th><?= __('Questions') ?></th>                   
                    <th><?= __('Class') ?></th>                   
                    <th><?= __('Action') ?></th>                    
                </tr>
            </thead>
            <tbody>
                <?php foreach ($notes as $note) {
                    ?>
                    <tr class="gradeU">
                        <td><?= $note->id ?></td>
                        <td><?= $note->chapter?></td>
                        <td><?= $note->title ?></td>
                        <td><?= $note->notes_eng ?></td>
                        <td><?= $note->notes_hindi ?></td>
                        <td><?= $note->questions ?></td>
                        <td><?= $note->class ?></td>
                        <td class="center"> 
                            <a class="btn btn-info" title="<?= __('Edit') ?>" href="<?= $this->Url->build(['controller' => 'Notes', 'action' => 'edit', $note->id]) ?>" >Edit</a>
                            <a href="<?= $this->Url->build(['controller' => 'Subjects', 'action' => 'delete', $note->id, $this->request->getParam('_csrfToken')]) ?>" class="btn btn-danger" title="<?= __('Delete notes') ?>" onclick="if (confirm('Do you really want to delete this subject?')) {
                                            return true;
                                        }
                                        return false;">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            <tbody>
        </table>
        <div class="pagination" style="float: right">
            <?php echo $this->Paginator->prev('' . ('<')) ?>
            <?php echo $this->Paginator->numbers(); ?>
            <?php echo $this->Paginator->next('' . ('>')) ?>
        </div>
    </div>    
</div>
