<div class="container" style="min-height: 80%">
    <div class="col-sm-10">
        <fieldset>
            <legend>Add Notes</legend>
            <div class="form-group">
                <?= $this->Form->create('notes',array('name' => 'update')); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->select('class', $class, ['class' => 'form-control','empty'=>'Select Class']); ?>
            </div>            
            <div class="form-group">
                <?= $this->Form->select('sub_id', $subjects, ['class' => 'form-control','empty'=>'Select Subject']); ?>
            </div>                        
            <div class="form-group">
                <?= $this->Form->select('chapter', $chapters, ['class' => 'form-control','empty'=>'Select Chapter']); ?>
            </div>                        
            <div class="form-group">
                <?= $this->Form->control('title', ['class' => 'form-control']) ?>
            </div>            
            <div class="form-group">
                <?= $this->Form->control('notes', ['class' => 'form-control','type'=>'textarea','id'=>'editor1']) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('questions', ['class' => 'form-control','type'=>'textarea']) ?>
            </div>            
            <div class="form-group">
                <?= $this->Form->button('Add', ['class' => 'btn btn-primary']); ?>
            </div>
        </fieldset>
    </div>
</div>

