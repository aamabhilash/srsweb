<div class="container">
    <div class="col-sm-10">
        <fieldset>
            <legend>Edit Testimonial</legend>
            <table>
                <div class="form-group" >
                    <?= $this->Form->create($testimonial, array('name' => 'add', 'onsubmit' => 'return add_validation()', 'type' => 'file')); ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->textarea('testimonial_content', ['label' => __('Comment'), 'rows' => '5', 'class' => 'form-control']); ?>
                </div>
		 <div class="form-group">
                    <?= $this->Form->control('rating', ['label' => __('Rating'), 'class' => 'form-control']); ?>
                </div>
		<div class="form-group">
                    <?= $this->Form->control('submited_by', ['label' => __('Submit By'), 'class' => 'form-control']); ?>
		    
                </div>
                <div class="form-group">
                    <?= $this->Form->button('Update', ['class' => 'btn btn-primary']); ?>
                </div>
                <?= $this->Form->end() ?>
            </table>
        </fieldset>
    </div>
</div>


