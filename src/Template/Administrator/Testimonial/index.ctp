<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
 <a class="btn btn-info" href="<?= $this->Url->build(["controller" => "Testimonial", "action" => "add"]); ?>" style="float: right;margin-bottom: 10px;">Add New Testimonial</a>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th  style='width:10%;'><?= __('ID') ?></th>
                    <th style='width:50%;'><?= __('Comment') ?></th>
                    <th style='width:10%;'><?= __('Rating') ?></th>
                    <th style='width:10%;'><?= __('Submited By') ?></th>
                    <th style='width:20%;'><?= __('Action') ?></th>                    
                </tr>
            </thead>
            <tbody>
                <?php foreach ($testimonial as $data) {
                    ?>
                    <tr class="gradeU">
                        <td><?= $data->id ?></td>
                        <td><?= $data->testimonial_content ?></td>
                        <td><?= $data->rating ?></td>
                        <td><?= $data->submited_by ?></td>
                        <td class="center">   
                            <a class="btn btn-info" title="<?= __('Edit') ?>" href="<?= $this->Url->build(['controller'=>'Testimonial','action'=>'edit',$data->id]) ?>" ><i class="entypo-pencil"></i> <?= __('Edit') ?></a>
                            <a href="<?= $this->Url->build(['controller' => 'Testimonial', 'action' => 'delete', $data->id]) ?>" class="btn btn-danger" title="<?= __('Delete Testimonial') ?>" onclick="if (confirm('Do you really want to delete this testimonial?')) {
                                        return true;
                                    }
                                    return false;"><i class="entypo-cancel"></i> <?= __('Delete') ?></a>
                        </td>
                    </tr>
                <?php } ?>
            <tbody>
        </table>
    </div>
</div>