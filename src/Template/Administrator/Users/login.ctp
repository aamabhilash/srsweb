
    <div class="login-box">
        <div class="login-logo">
            <a href="../../index2.html"><b>Admin</b>LTE</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

           <?php echo $this->Form->create('LoginForm', array('name' => 'login', 'onsubmit' => 'return validate_login()', 'type' => 'post')); ?>
                <div class="form-group has-feedback">
                   <?= $this->Form->input('email', ['class' => 'form-control', 'placeholder' => 'Email', 'label' => False ]) ?>

                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?= $this->Form->input('password', ['class' => 'form-control', 'placeholder' => 'Password', 'label' => False ]) ?>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                       
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            <?php echo $this->Form->end(); ?>

            <div class="social-auth-links text-center">
              
            </div>
            <!-- /.social-auth-links -->
           

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>


