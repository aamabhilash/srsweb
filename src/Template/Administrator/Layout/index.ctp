<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Neon Admin Panel" />
        <meta name="author" content="" />
        <?= $this->Html->charset() ?>
        <title>
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->css(array('Adm/bootstrap.min', 'Adm/font-awesome.min.css', 'Adm/ionicons.min.css', 'Adm/AdminLTE.min.css', 'Adm/blue.css')) ?>
        <?= $this->Html->css('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic') ?>
    </head>
    <body class="hold-transition login-page">
        <?php echo $this->Flash->render() ?>
        <?php echo $this->fetch('content'); ?>
        <?= $this->Html->script(array('/js/Adm/jquery.min.js','Adm/bootstrap.min.js', 'Adm/icheck.min')) ?>
    </body>
</html>
