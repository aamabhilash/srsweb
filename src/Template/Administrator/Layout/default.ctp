<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Admin';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>

        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php echo $this->Html->css(array('Adm/bootstrap.min', 'Adm/font-awesome.min.css', 'Adm/ionicons.min.css', 'Adm/AdminLTE.min.css', 'Adm/_all-skins.min','Adm/bootstrap3-wysihtml5.min')) ?>
        <?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic') ?>
        <?php echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php if (!(isset($this->request->controller) == 'Users' && in_array($this->request->action, array('login')))) { ?>
            <div class="wrapper"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->


                <header class="main-header">

                    <!-- Logo -->
                    <a href="index2.html" class="logo">
                        <!-- mini logo for sidebar mini 50x50 pixels -->
                        <span class="logo-mini"><b>A</b>LT</span>
                        <!-- logo for regular state and mobile devices -->
                        <span class="logo-lg"><b>Admin</b>LTE</span>
                    </a>

                    <!-- Header Navbar: style can be found in header.less -->
                    <nav class="navbar navbar-static-top">
                        <!-- Sidebar toggle button-->
                        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                            <span class="sr-only">Toggle navigation</span>
                        </a>                      
                    </nav>
                </header>
                <aside class="main-sidebar">
                    <section class="sidebar">
                        <?php echo $this->element('left'); ?>
                    </section>
                </aside>
                <div class="content-wrapper">
                    <?= $this->element('header'); ?>
                   <?= $this->Flash->render() ?>
                <?php } ?>
                <?php echo $this->fetch('content'); ?>
                <?php if (!(isset($this->request->controller) == 'Users' && in_array($this->request->action, array('login')))) { ?>
                   
                </div>
                <!-- Chat Histories -->

            </div>
        <?php } ?>
        <?php echo $this->Html->script(array('Adm/jquery.min', 'Adm/bootstrap.min', 'Adm/jquery.form-validator.min','Adm/fastclick.js', 'Adm/adminlte.min.js', 'Adm/jquery.sparkline.min.js', 'Adm/jquery-jvectormap-1.2.2.min', 'Adm/jquery-jvectormap-world-mill-en', 'Adm/jquery.slimscroll.min', 'Adm/Chart', 'Adm/dashboard2', 'Adm/demo','Adm/jquery-ui','Adm/custom.js','Adm/ckeditor','Adm/bootstrap3-wysihtml5.all.min.js')); ?>
        <?php echo $this->fetch('script') ?>
        <?php echo $this->fetch('footerscript') ?>
         <script>
            $(function () {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor1')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            })
        </script>
    </body>
</html>
