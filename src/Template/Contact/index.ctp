
<div class="map-main">    
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3500.415818647536!2d77.08468226562357!3d28.677205482400264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d046cf7551e91%3A0xfa7123b04828edff!2sMianwali+Nagar%2C+Paschim+Vihar%2C+Delhi%2C+110087!5e0!3m2!1sen!2sin!4v1505289950889" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>   
</div>

<section id="main-container">

    <div class="container">
        <div class="get-in-touch">
	    <div class="row">
		<div class="col-sm-6">
		    <div class="left-side-c">
			<h2>Get in Touch</h2>
			<?= $this->Form->create('contact', array('name' => 'add')); ?>
			<div class="form-group">
			    <?= $this->Form->control('name', ['class' => 'form-control', 'placeholder' => 'Name','label'=>false]); ?>
			</div>
			<div class="form-group">
			    <?= $this->Form->control('email', [ 'class' => 'form-control', 'placeholder' => 'Email Address','label'=>false]); ?>
			</div>
			<div class="form-group">
			    <?= $this->Form->textarea('message', ['rows' => '5', 'class' => 'form-control', 'placeholder' => 'Message here..','label'=>false]); ?>
			</div>
			<div class="form-group">
			    <?= $this->Form->button('Submit', ['class' => 'btn btn-primary']); ?>
			</div>
			<?= $this->Form->end() ?>

		    </div>
		</div>
		<div class="col-sm-6">
		    <div class="right-side-c">
			<h2>Our address</h2>
			<h4><span>Our Location</span>
			    C-5/89-90, Second Floor, Sector-6, 
			    Rohini, New Delhi-110085, India</h4>

			<h4><span>Phone no.</span>
			    +91 8527118181, </h4>

			<h4><span>Email Address</span>
			    Companyname@gmail.com</h4>

			<h4><span>Follow us:-</span></h4>

		    </div>
		</div>
	    </div>
        </div>
    </div>
</section>