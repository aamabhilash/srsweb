
<div class="banner-half">
     <div class="container">
       <h1>About us</h1>
       <h5>Salvia next level crucifix pickled heirloom synth</h5>
     </div>
   </div>
  
    <section id="main-container"> 
     
      <div class="container">
        <div class="our-story">
          <div class="col-md-6 col-sm-12 benefit-left no-pad">
            <div class="benefit-text no-pad">
              <h2>About Learning Point Home Tutors</h2>
              <p>Learning Point began as an educational institute 10 years back, so we know what it takes to be the best. We understand the requirements of students as well as the tutors. Since its inception it is growing day by day and is a leading home tuition provider that offers quality education to the students of Delhi at their place. We believe that best education should be provided to students and in pursue of our aim we are working harder and harder day by day. </p>
              
              <p>We are renowned for our superior quality service, individual attention according to the requirement of each student, in-depth knowledge of subject which makes us as one of the most trusted home tutors throughout the region and has proven its success time and again. </p>
            </div>
          </div>
        </div>
        
        <div class="our-mission">
        
        <div class="col-md-6 col-sm-12 benefit-left no-pad our-misn">
        <div class="benefit-text no-pad ">
         <h2>Vision, <br>
            Mission and <br>
            values</h2>
        </div>
        </div>
        
          <div class="col-md-6 col-sm-12 benefit-right no-pad">
            <div class="benefit-text no-pad">
              <h2>Our Story</h2>
              <p>Learning Point Home Tutors has a mission to become a pioneer in our field and be the best by the end of 2020. 
Our objective is to run an efficient service in order to provide individual tuition tailored to pupils' specific needs in a vast range of subjects, levels and age groups
To provide total educational solutions to the students and help home tutors to find best tuitions according to their needs.
</p>

<p>We strive to make our students excel in the field by raising their aspiration levels by infusing them with self-confidence, right direction and motivation so that they can achieve their desired goals.</p>
            </div>
          </div>
        </div>
        
      </div>
      
      <div class="container">
        <div class="payment-info">
          <div class="row">
            <div class="col-sm-7">
             <div class="pay-info-inn">
               <h2> Bank Info : </h2>
                <span>Account Holder Name </span>: Gunjan Taneja<br>
                <span>Account No</span> : 32870084787<br>
                <span>Bank </span>: State Bank Of India<br>
                <span>Branch</span> : Mianwali Nagar<br>
                <span>IFSC Code</span> : SBIN0016202<br>
             </div>
            </div>
            
            <div class="col-sm-5">
             <div class="pay-info-inn">
               <h2>Paytm no  : </h2>
                <img src="<?= $this->request->base ?>/img/Paytm-Logo.png">
                <span>+91-9873850198</span>
             </div>
            </div>
            
          </div>
        </div>
      </div>
      
           
    </section>