
<div class="banner-half">
     <div class="container">
       <h1>Terms and conditions</h1>
       <h5>Salvia next level crucifix pickled heirloom synth</h5>
     </div>
   </div>
  
    <section id="main-container"> 
     
      <div class="container">
      <div class="term-condition">
        <h2>For Tutors</h2>
        <ul>
          <li>50% of the first month's fee will be charged as commission.</li>
         <li>30% commission will be charged for package based/Short term courses.</li>
         <li>First payment will be collected by the officials of <b>Learning Point Home Tutors.</b></li>
         <li>Maximum of three chances will be given to the tutor to prove themselves.</li>
         <li>Future assignments will solely depend on the performance of the tutor.</li>
         <li><b>Learning Point Home Tutors</b> adheres to the highest standards of academic rigor in all its work .Any lapse on the part of the tutor will lead to the immediate elimination of his/her name from <b>Learning Point Home Tutors</b>.</li>
         <li>As a home tutor, you understand to follow with all applicable laws, exercise of professional competence, care, skill, diligence and prudence as is usually exercised by professionals.</li>
         <li>You be aware of that the final decision to select a particular student for tuition is yours and yours alone.</li>
         <li>You Know and agree that you will teach the student if and only if at least one parent is present in the house.</li>
         <li>We will not responsible for any inconvenience, miss happening by the parents.</li>
         <li>You understand that all information provided is deemed reliable but is not guaranteed and should be independently verified.</li>
         <li>You understand that if you agreed to take an assignment, you have to complete it. In unavoidable circumstances you have to give minimum 15 days notice period.</li>
         <li>You have to arrange demo class within 48 hours of receiving details of Tuition. If this does not happen due to any reason, you have to inform us immediately.</li>
         <li>Tutor Registration can be cancelled without notification on :-
           <ol>
            <li>Any misconduct to the Client or us.</li>
            <li>Consecutive Failure of 2 assignments by any reason. 3rd chance may be considered in specific circumstances.</li>
            <li>Absentee for a long duration without notice.</li>
           </ol>
         </li>
         </ul>
         
         <h2>The tutor needs to submit the following documents:</h2>
         <ul>
         <li>One pass port size photograph.</li>
         <li>Copy of your address proof (For eg Aadhar Card,Driving license, Passport, Voter's ID card)</li>
         <li>Copy of your Highest/Latest degree.</li>
         <li>Tutor registration charges Rs.1000/- (Valid for one year from date of Joining)</li>
         </ul>
         
          <h2>Student</h2>
          <strong>Parents are advised to keep following points in mind:</strong>
         <ul>
          <li>Take the address proof and a photograph of the tutor. <b>Learning Point Home Tutors</b> will not be responsible for any kind of mishap that takes place.</li>
          <li>Payment should not be given in advance to the tutor, <b>Learning Point Home Tutors</b> will not be responsible for any kind of dispute regarding the fee between the two.</li>
          <li>You understand that in the case where the tutor fails to deliver all or part of the scheduled tuitions, we will assist finding a replacement tutor for your child within a reasonable period of time.</li>
          <li>You be aware of that home tutors need to be treated with respect and dignity. Further, you agree to provide in your home an environment conducive to one on one study during the currency of the said home tuition.</li>
          <li>You be aware of that the final decision to select a particular tutor for your child is yours and yours alone.</li>
          <li>You know that the tutor will teach your child if and only if at least one parent is present in the house.</li>
          <li>We will not responsible for any inconvenience, miss happening by the tutors.</li>
          <li>If parent pay the tuition fee directly to tuition tutor in cash then we will not be responsible for that tuition fee.</li>
          <li>You understand that all information provided is deemed reliable but is not guaranteed and should be independently verified.</li>
          <li>We will not be responsible for any advance payment paid to Tuition Tutor. There is no role of organization from 2nd month in fee matter.</li>
          <li>Any dispute subject to Delhi jurisdiction only.</li>
           </ul>
          <h2>TERMS AND CONDITIONS SUBJECT TO CHANGE</h2>
          <strong>We reserve the right to update or modify these Terms of Use at any time without prior notice. Your access and use of this Website following any such change constitutes your agreement to follow and be bound by these Terms of Use as updated or modified. Therefore, we recommend you to review these Terms of Use every time you access and use this Website</strong>
       
       </div>
      </div>
    </section>