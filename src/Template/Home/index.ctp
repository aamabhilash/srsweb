<div class="main-slider">
    <div class="banner-tab">    
	<div class="container">
	    <h1>Need a Tutor or Coaching Class?</h1>
	    <!-- Nav tabs -->
	    <ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Register as Tutor</a></li>
		<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Register as student</a></li>
	    </ul>

	    <!-- Tab panes -->
	    <div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home" data-title="Home">
		    <?= $this->Form->create('teacher', array('name' => 'add', 'id'=>'teacher-user','class' => 'form-inline','url'=>array('controller'=>'teacher','action'=>'index'))); ?>
		    <div class="row">
			<div class="col-sm-3">
			    <div class="form-group">
				<?= $this->Form->input('name', [ 'class' => 'form-control','label'=>false,'placeholder'=>'Name','data-validation' => 'required', 'data-validation-error-msg' => 'Enter Name']); ?>
			    </div>
			</div>
			<div class="col-sm-3">
			    <div class="form-group">
				<?= $this->Form->input('mobile_no', [ 'class' => 'form-control','label'=>false,'placeholder'=>'Mobile No.','data-validation' => 'required', 'data-validation-error-msg' => 'Enter Mobile Number']); ?>
			    </div>
			</div>
			<div class="col-sm-3">
			    <div class="form-group">
				<?= $this->Form->select('locality' ,['label' =>__('Locality')],['data-validation' => 'required', 'data-validation-error-msg' => 'Select Locality']); ?>
			    </div>
			</div>
			<div class="col-sm-3">
				<?= $this->Form->button('Submit', ['class' => 'btn btn-primary']); ?>
			    </div>
		    </div>
		    <?= $this->Form->end() ?>		  
		</div>		
	    </div>
	</div>
    </div>
    <div id="owl-demo" class="owl-carousel owl-theme">       
	<div class="item">
	    <img   src="<?= $this->request->base ?>/img/rajkot-mahila-college-1.jpg" alt="The Last of us">
	</div>
    </div>
</div>

<?php echo  $this->Html->scriptStart(); ?>

$.validate({
form: '#student-user, #teacher-user'
});
<?= $this->Html->scriptEnd(); ?>