
<section id="main-container" class="student-info"> 

    <div class="container">
	<div class="student-form-box">

	    <?= $this->Form->create('', array('name' => 'teacher_info', 'type' => 'file','url'=>array('controller'=>'teacher','action'=>'addteacher'))); ?>
	    <h2 class="text-center">Teacher information</h2>

	    <div class="form-group">
		<div class="row">
                    <div class="col-sm-6 margin-m">
			<div class="cols-sm-10">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
				<?= $this->Form->input('name', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Teacher Name']); ?>

			    </div>
			</div>
		    </div>

		    <div class="col-sm-6">
			<div class="cols-sm-10">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-user-plus" aria-hidden="true"></i></span>
				<?= $this->Form->input('mobile_no', ['label' => false, 'class' => 'form-control', 'placeholder' => "Mobile Number"]); ?>

			    </div>
			</div>
		    </div>             
                </div>
	    </div>

	    <div class="form-group">
                <div class="row">
                    <div class="col-sm-6 margin-m">
			<?= $this->Form->select('subject', ['label' => __('Subject You Can Teach')]); ?>
		    </div>

		    <div class="col-sm-6">
			<?= $this->Form->select('class', ['label' => __('Class You Can Teach')]); ?>
		    </div>
                </div>
	    </div>


	    <div class="form-group">
                <div class="row">
                    <div class="col-sm-6 margin-m">
			<div class="cols-sm-10">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-mobile" style="font-size:24px;" aria-hidden="true"></i></span>
				<?= $this->Form->input('qualification', ['label' => false, 'class' => 'form-control', 'placeholder' => "Qualification"]); ?>

			    </div>
			</div>
		    </div>

		    <div class="col-sm-6">
			<div class="cols-sm-10">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-phone" style="font-size:20px;" aria-hidden="true"></i></span>
				<?= $this->Form->input('experience', ['label' => false, 'class' => 'form-control', 'placeholder' => "Experience"]); ?>
			    </div>
			</div>
		    </div>
                </div>
	    </div>
	    <div class="form-group">
                <div class="row">

		    <div class="col-sm-6">
			<div class="cols-sm-10">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-phone" style="font-size:20px;" aria-hidden="true"></i></span>
				<?= $this->Form->input('age', ['label' => false, 'class' => 'form-control', 'placeholder' => "Age"]); ?>
			    </div>
			</div>
		    </div>
                    <div class="col-sm-6 margin-m">
			<?= $this->Form->select('subject', ['label' => __('Preferred Teaching Area')]); ?>
		    </div>
                </div>
	    </div>



	    <div class="form-group">
		<div class="cols-sm-10">
		    <div class="row">
			<div class="col-sm-8 margin-m">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-map-marker" style="font-size:20px;" aria-hidden="true"></i></span>
				<?= $this->Form->input('address', ['label' => false, 'class' => 'form-control', 'placeholder' => "Address"]); ?>
			    </div> 
			</div>

                        <div class="col-sm-4">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
				<?= $this->Form->input('pincode', ['label' => false, 'class' => 'form-control', 'placeholder' => "Pin Code", 'default' => '1100']); ?>
			    </div> 
			</div>

		    </div>
		</div>
	    </div>
	    <!-- File Uploading  -->
	    <div class="form-group">
		<div class="cols-sm-10">
		    <div class="row">
			<div class="col-sm-6 margin-m">
			    <?php
			    echo $this->Form->input('front_side_aadhar_card', [
				'templates' => [
				    'inputContainer' => '<span class="input file required input {{type}}{{required}}"><span>Aadhar Card Front Side</span>{{content}}</span>',
				],
				'onchange' => 'onFileImage(this);',
				'class' => 'form-control',
				'type' => 'file',
				'label' => false
			    ]);
			    ?>
			</div>
			<div class="col-sm-6 margin-m">
			    <?php
			    echo $this->Form->input('back_side_aadhar_card', [
				'templates' => [
				    'inputContainer' => '<span class="input file required  input {{type}}{{required}}"><span>Aadhar Card Back Side</span>{{content}}</span>',
				],
				'onchange' => 'onFileImage(this);',
				'class' => 'form-control',
				'type' => 'file',
				'label' => false
			    ]);
			    ?>
			</div>
		    </div>
		</div>
	    </div>

	    <!-- End -->
	    <div class="form-group radio-line">
                <div class="row">
		    <label class="col-xs-4 control-label" for="IsSmallBusiness">Gender</label>

		    <div class="col-xs-8 tutor-preferred">
<?= $this->Form->radio('gender', ['Male', 'Female'], array('class' => 'radio-inline')); ?>
		    </div>
                </div>
            </div>



	    <div class="form-group ">
<?= $this->Form->button('Submit', ['class' => 'btn btn-primary']); ?>
	    </div>
		<?= $this->Form->end() ?>
	</div>
    </div>
</section>