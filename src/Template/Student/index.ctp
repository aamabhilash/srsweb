

<section id="main-container" class="student-info"> 

    <div class="container">
	<div class="student-form-box">

	    <?= $this->Form->create('', array('name' => 'student_info','id'=>'student','url'=>array('controller'=>'student','action'=>'addstudent'))); ?>
	    <h2 class="text-center">Student information</h2>

	    <div class="form-group">
		<div class="cols-sm-10">
		    <div class="row">
			<div class="col-sm-3">
			    <label style="line-height:40px;">no of student</label>
			</div>
			<div class="col-sm-3">
			   <?= $this->Form->select('total_student', ['No of student',1, 2, 3, 4, 5]); ?>
			</div>
		    </div>  
		</div>
	    </div>

	    <div class="form-group">
		<div class="row">
                    <div class="col-sm-6 margin-m">
			<div class="cols-sm-10">
			    <div class="input-group">
				<input type='hidden' name='ip' value='<?= time();?>'>
				<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
				<?= $this->Form->control('name', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Student Name']); ?>

			    </div>
			</div>
		    </div>

		    <div class="col-sm-6">
			<div class="cols-sm-10">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-user-plus" aria-hidden="true"></i></span>
				<?= $this->Form->control('parent_name', ['label' => false, 'class' => 'form-control', 'placeholder' => "Parent's Name"]); ?>

			    </div>
			</div>
		    </div>             
                </div>
	    </div>


	    <div class="form-group">
                <div class="row">
                    <div class="col-sm-6 margin-m">
			<div class="cols-sm-10">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-mobile" style="font-size:24px;" aria-hidden="true"></i></span>
				<?= $this->Form->control('mobile_no', ['label' => false, 'class' => 'form-control', 'placeholder' => "Mobile Number"]); ?>

			    </div>
			</div>
		    </div>

		    <div class="col-sm-6">
			<div class="cols-sm-10">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-phone" style="font-size:20px;" aria-hidden="true"></i></span>
				<?= $this->Form->control('phone_no', ['label' => false, 'class' => 'form-control', 'placeholder' => "Phone Number"]); ?>
			    </div>
			</div>
		    </div>
                </div>
	    </div>

	    <div class="form-group">
                <div class="row">
                    <div class="col-sm-6 margin-m">
			<?= $this->Form->select('class', ['label' => __('Class for which you want tution')]); ?>
		    </div>

		    <div class="col-sm-6">
			<?= $this->Form->select('subject', ['label' => __('Subject for which you want tution'),'label']); ?>
		    </div>
                </div>
	    </div>


	    <div class="form-group">
		<div class="cols-sm-10">
		    <div class="row">
			<div class="col-sm-8 margin-m">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-map-marker" style="font-size:20px;" aria-hidden="true"></i></span>
				<?= $this->Form->control('address', ['label' => false, 'class' => 'form-control', 'placeholder' => "Addresss"]); ?>
				
			    </div> 
			</div>

                        <div class="col-sm-4">
			    <div class="input-group">
				<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
				<?= $this->Form->control('pincode', ['label' => false, 'class' => 'form-control','default'=>'1100']); ?>
			    </div> 
			</div>

		    </div>
		</div>
	    </div>



	    <div class="form-group radio-line">
                <div class="row">
                <label class="col-xs-4 control-label" for="IsSmallBusiness">Tutor Preferred</label>
		
	
                <div class="col-xs-8 tutor-preferred">
                   <?= $this->Form->radio('tutor_preferred', ['Male','Female','Any'],array('class'=>'radio-inline')); ?>
                </div>
                </div>
            </div>

	    <div class="form-group ">
		<?= $this->Form->button('Submit', ['class' => 'btn btn-primary']); ?>
	    </div>
	    <?= $this->Form->end() ?>
	</div>
    </div>
</section>