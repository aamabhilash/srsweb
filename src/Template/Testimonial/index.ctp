<div class="banner-half">
    <div class="container">
	<h1>Testimonials</h1>
	<h5>Salvia next level crucifix pickled heirloom synth</h5>
    </div>
</div>

<section id="main-container" class="testimonial-container"> 

    <div class="container-fluid">
	<div class="testimonials-page">
	    <div id="timeline" data-columns>

		<?php
		if (isset($testimonial)) {
		    foreach ($testimonial as $data) {
			?>
			<div class="item tes_item">
			    <div class="testi-info">
				<p><span class="rular_line"><i class="fa fa-quote-left"></i></span></p>
				<p> <?= $data->testimonial_content ?></p>
				<p><span class="rular_line"><i class="fa fa-quote-right"></i></span></p>

				<div class="stars ratingsForm">
				    <div class="Fr-star userChoose size-3"  data-rating="<?= $data->rating ?>">
					<div class="Fr-star-value" style="width:<?= $data->rating * 20 ?>%;"></div>
					<div class="Fr-star-bg"></div>
				    </div>
				</div>


				<ul class="cd-author-info">
				    <li><span>submitted by</span> &nbsp; <?= $data->submited_by ?></li>
				</ul>
			    </div>
			</div>

		    <?php
		    }
		}
		?>
	    </div>
	</div>
    </div>
</section>