<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>learning point</title>
	<?php echo $this->Html->css(array('bootstrap.min', 'Fr.star','https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', 'https://fonts.googleapis.com/css?family=Montserrat:400,500', 'style')) ?>
    </head>

    <body>

	<!-- Navigation -->
	<nav class="navbar navbar-default megamenu" id="nav">
	    <div class="too-bar"></div>
	    <div class="container">

		<div class="top-info-header">
		    <span><i class="fa fa-envelope"></i> &nbsp; learningPoint@gmail.com</span>
		    <span><i class="fa fa-phone"></i> &nbsp; +(011) 345 6789</span>
		    <div class="top-social">
			<a class="facebook"><i class="fa fa-facebook"></i></a>
			<a class="facebook"><i class="fa fa-twitter"></i></a>
			<a class="facebook"><i class="fa fa-google"></i></a>
			<a class="facebook"><i class="fa fa-youtube"></i></a>
		    </div>
		</div>

		<div class="navbar-header">
		    <button type="button" class="navbar-toggle toggle-menu menu-left push-body" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" rel="home" href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'index']) ?>" title="Buy Sell Rent Everyting">
			<img  class="img-responsive" src="<?= $this->request->base ?>/img/logo.png">
			
		    </a>
		</div>

		<div class="collapse navbar-collapse cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="bs-example-navbar-collapse-1">
		    <ul class="nav navbar-nav navbar-right">
			<li class="active"><a href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'index']) ?>">Home</a></li>
			<li><a href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'ourTutor']) ?>">Our tutors</a></li>
			<li><a href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'about']) ?>">About us</a></li>
			<li><a href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'termsAndCondition']) ?>">Terms & conditions</a></li>
			<li><a href="<?= $this->Url->build(['controller' => 'Notes', 'action' => 'index']) ?>">Notes</a></li>
			<li><a href="<?= $this->Url->build(['controller' => 'Contact', 'action' => 'index']) ?>">Contact Us</a></li>
		    </ul>
		</div>

	    </div>
	</nav>
	<?= $this->Flash->render() ?>
	<div class="clearfix">
	    <?= $this->fetch('content') ?>
	</div>

	<!-- footer html -->
<!--	<div class="working-hour">
	    <div class="container">
		<div class="col-sm-12">
		    <div class="counter_block">
			<div class="counter">Working Hour</div>
			<div class="member_tutor">Mon-Sat : 10am to 8pm </div>
		    </div>
		</div>

	    </div>
        </div>-->
	
	<div class="footer">
	    <div class="ftr-third-row ftr-row">
		<div class="container">

		    <div class="ftr-copyright">
			Copyright © 2016 - 2017 Learning point. All rights reserved
		    </div>

		    <div class="ftr-terms">
			<a href="#">Site Map</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
			<a href="#">Terms of Use</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
			<a href="#">Privacy Policy</a>
		    </div>

		</div>
	    </div>

	</div>


	<?php echo $this->Html->script(array('jPushMenu.js', 'jquery-3.2.1.min.js','jquery.form-validator.min', 'bootstrap.min.js', 'salvattore.min.js','custom','Fr.star')); ?>

	

    </body>

</html>
