<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\ORM\RulesChecker;

class TestimonialTable extends Table {
    
    public function ValidationDefault(Validator $validator) {
        $validator->notEmpty('testimonial_content', 'Comment is required');        
        $validator->notEmpty('submited_by', 'Field is required');
     
        return $validator;
    }
}
