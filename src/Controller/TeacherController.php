<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class TeacherController extends AppController {

    public function index() {
	if ($this->request->is('post') && isset($this->request->data['locality'])) {

	    $name = $this->request->data['name'];
	    $mobile_no = $this->request->data['mobile_no'];
	    $locality = $this->request->data['locality'];
	}
	$this->set(compact('name', 'mobile_no', 'locality'));
    }

    public function addteacher() {
	
	
	$teacherTable = TableRegistry::get('Teacher');
	$teacher = $teacherTable->newEntity();
	if ($this->request->is('post')) {
	    $front_image = $this->saveimage($this->request->data['front_side_aadhar_card'],'front');
	     $back_image = $this->saveimage($this->request->data['back_side_aadhar_card'],'back');
	    $data = $this->request->data;
	    $data['front_side_aadhar_card']=$front_image;
	    $data['back_side_aadhar_card']=$back_image;
	    $teacherTable->patchEntity($teacher, $data);
	    if ($teacherTable->save($teacher)) {
		$this->Flash->success(__('Teacher added successfully'));
		return $this->redirect(array('controller' => 'Home', 'action' => 'index'));
	    } else {
		$this->Flash->success(__('Please Try Again'));
		return $this->redirect(array('controller' => 'Teacher', 'action' => 'index'));
	    }
	}
    }
    
    public function saveimage($data ,$val) {
    $temp_name = $data["tmp_name"];
    $imgtype = $data["type"];
    $upload_path='';
    if($val=='front'){
	$upload_path=FRONT_SIDE_IDENTITY_IMAGE;
    }elseif($val=='back'){
	$upload_path=BACK_SIDE_IDENTITY_IMAGE;
    }
    $ext = $this->getImageExtension($imgtype);

    if (!$ext) {
        return false;
    }
    $imagename = date("d-m-Y") . "-" . time() . $ext;

    $target_path = $upload_path . $imagename;

    if (copy($temp_name, $target_path)) {

        return $imagename;
    } else {
        return false;
    }
}

 public function getImageExtension($imagetype) {
    if (empty($imagetype)) {
        return false;
    }
    switch ($imagetype) {
        
        case 'image/jpg': return '.jpg';
        case 'image/png': return '.png';
        case 'image/jpeg': return '.jpg';
        default: return FALSE;
    }
}

}
