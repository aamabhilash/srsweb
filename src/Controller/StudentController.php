<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class StudentController extends AppController {

    public function index() {

	if ($this->request->is('post')) {
	    $name = $this->request->data['name'];
	    $mobile_no = $this->request->data['mobile_no'];
	    $locality = $this->request->data['locality'];
	}
	$this->set(compact('name', 'mobile_no', 'locality'));
    }

    public function addstudent() {
	$studentTable = TableRegistry::get('Student');
	$student = $studentTable->newEntity();

	if ($this->request->is('post') && !empty($this->request->data['name'])) {

	    $data = $this->request->data;
	    $studentTable->patchEntity($student, $data);
	    if ($studentTable->save($student)) {
		$this->Flash->success(__('Syudent added successfully'));


		$session = $this->request->session();
		$session->read('noOfStudent');
		if (isset($this->request->data['total_student']) && $this->request->data['total_student'] == '1' || $this->request->data['total_student'] == '0') {
		    return $this->redirect(array('controller' => 'Home', 'action' => 'index'));
		} elseif ($this->request->data['total_student'] > 1) {
		    if (isset($session->read('noOfStudent'))) {
			$session->write('noOfStudent', $this->request->data['total_student'] - 1);
		    } elseif ($this->request->data['total_student'] == '1') {
			
		    }


		    return $this->redirect(array('controller' => 'Student', 'action' => 'index'));
		} else {
		    return $this->redirect(array('controller' => 'Home', 'action' => 'index'));
		}
	    } else {
		return $this->redirect(array('controller' => 'Student', 'action' => 'index'));
	    }
	}
    }

}
