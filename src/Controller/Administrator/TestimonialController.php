<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Administrator;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Schema\Table;

/**
 * CakePHP Testimonial
 * @author Amility
 */
class TestimonialController extends AppController {

    public function index() {
	$testimonialTable = TableRegistry::get('Testimonial');
	$testimonial = $testimonialTable->find('all')->order(['id' => 'desc']);
	$this->set(compact('testimonial'));
    }

    /**
     * Add New Testimonial Content
     * 
     */
    public function add() {

	$testimonialTable = TableRegistry::get('Testimonial');
	$testimonial = $testimonialTable->newEntity();
	if ($this->request->is('post')) {

	    $data = $this->request->data;
	    $testimonialTable->patchEntity($testimonial, $data);
	    if ($testimonialTable->save($testimonial)) {
		$this->Flash->success(__('Task added successfully'));
	    }
	}

	$this->set(compact('testimonial'));
    }

    /**
     * 
     * @param type $id
     * Edit Testimonial content
     */
    public function edit($id) {
	$testimonialTable = TableRegistry::get('Testimonial');
	$testimonial = $testimonialTable->get($id);
	if ($this->request->data) {
	    $data = $this->request->data;
	    if (!empty($id)) {
		$testimonialTable->patchEntity($testimonial, $data);
		if ($testimonialTable->save($testimonial)) {
		    $this->Flash->success('Testimonial updated successfully');

		    return $this->redirect(array('controller' => 'Testimonial', 'action' => 'index'));
		} else {
		    $this->Flash->error('Testimonial cannot be uppdated');
		}
	    }
	}
	$this->set(compact('testimonial'));
    }

    /**
     * 
     * @param type $id
     */
    public function delete($id) {
	if ($id != NULL) {
	    $testimonialTable = TableRegistry::get('Testimonial');
	    $testimonial = $testimonialTable->get($id);
	    if ($testimonialTable->delete($testimonial)) {
		$this->Flash->success('Testimonial deleted successfully');
	    } else {
		$this->Flash->error('Testimonial cannot be deleted');
	    }
	    return $this->redirect(array('controller' => 'Testimonial', 'action' => 'index'));
	}
    }

}
