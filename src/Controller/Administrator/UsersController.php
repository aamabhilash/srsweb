<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Administrator;

use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;
use App\Controller\AppController;

class UsersController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
	parent::beforeFilter($event);
	$this->Auth->allow(array('login'));
    }

    public function index() {
	
    }

    public function login() {
//print_r($this->request->data);exit();
	if (!$this->Auth->user()) {
	    $this->viewBuilder()->setLayout('index');
	    if ($this->request->is('post')) {
		$admin = $this->Auth->identify();
		if ($admin) {
		    $this->Auth->setUser($admin);
		    return $this->redirect($this->Auth->redirectUrl());
		} else {
		    $this->Flash->error(__('Username and Password is invalid'));
		}
	    }
	} else {
	    return $this->redirect(['controller' => 'Users', 'action' => 'login']);
	}

    }

    public function dashboard() {
	
    }

     public function logout() {
        return $this->redirect($this->Auth->logout());
    }
}
