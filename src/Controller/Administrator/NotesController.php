<?php

namespace App\Controller\Administrator;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Schema\Table;
use Cake\Core\Configure;


class NotesController extends AppController {

    public $Notes;
    public $Subjects;

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Notes = TableRegistry::get('Notes');
        $this->Subjects = TableRegistry::get('Subjects');
    }

    public function index() {
        $conditions = array();
        if (!empty($this->request->query('chapter_name'))) {
            $conditions = ['title LIKE' => '%' . trim($this->request->query('chapter_name')) . '%'];
        }
        $notes = $this->paginate($this->Notes->find('all', ['order' => ['Notes.id' => 'DESC']])->where($conditions));
        $this->set(compact('notes'));
    }

    public function add() {
        $notes = $this->Notes->newEntity();        
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $this->Notes->patchEntity($notes, $data);
            if ($this->Notes->save($notes, $data)) {
                $this->Flash->success(__('Notes added successfully'));
                return $this->redirect(['controller' => 'Notes', 'action' => 'index']);
            }
        }
        $subjects = $this->Subjects->find('list')->select(['sub_id', 'title'])->toArray();        
        $class = Configure::read('Class');
        $chapters = Configure::read('Chapters');
        $this->set(compact(array('subjects','class','chapters')));
        
    }
    
    
        public function edit($id) {
        $notes = $this->Notes->get($id);
        $subjects = $this->Subjects->find('list')->select(['sub_id', 'title'])->toArray();
        $this->set(compact('subjects'));
        if (!empty($this->request->data)) {
            if (!empty($id)) {
                $this->Notes->patchEntity($notes, $this->request->data());
                if ($this->Subjects->save($notes)) {

                    $this->Flash->success('Data updated successfully');
                    return $this->redirect(['controller' => 'Notes', 'action' => 'index']);
                } else {
                    $this->Flash->error('Data cannot be uppdated');
                    return $this->redirect(['controller' => 'Notes', 'action' => 'index']);
                }
            }
        }
        $this->set(compact('notes'));
    }

}
