<?php

namespace App\Controller\Administrator;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class SubjectsController extends AppController {

    public $Subjects;

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Subjects = TableRegistry::get('Subjects');
    }

    public function index() {
        $conditions = array();
        if (!empty($this->request->query('sub_name'))) {
            $conditions = ['title LIKE' => '%' . trim($this->request->query('sub_name')) . '%'];
        }
        $subjects = $this->paginate($this->Subjects->find('all', ['order' => ['Subjects.sub_id' => 'DESC']])->where($conditions));
        $this->set(compact('subjects'));
    }
    
        public function add() {        
        $subject = $this->Subjects->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->data;            
            $this->Subjects->patchEntity($subject, $data);
            if ($this->Subjects->save($subject)) {
                $this->Flash->success(__('Subject added successfully'));
                return $this->redirect(['controller' => 'Subjects', 'action' => 'index']);
            }
        }        
    }
    
    /**
     * 
     * @param type $id
     * edit category
     */
    public function edit($id) {
        $subject = $this->Subjects->get($id);
        if (!empty($this->request->data)) {
            if (!empty($id)) {
                $this->Subjects->patchEntity($subject, $this->request->data());
                if ($this->Subjects->save($subject)) {

                    $this->Flash->success('Data updated successfully');
                    return $this->redirect(['controller' => 'Subjects', 'action' => 'index']);
                } else {
                    $this->Flash->error('Data cannot be uppdated');
                    return $this->redirect(['controller' => 'Subjects', 'action' => 'index']);
                }
            }
        }
        $this->set(compact('subject'));
    }

    /**
     * 
     * @param type $id
     * @param type $token
     * @return type
     * delete category action
     */
    public function delete($id, $token) {
        //check token before delete
        if ($token == $this->request->getParam('_csrfToken')) {
            if ($id != NULL) {                
                $subjects = $this->Subjects->get($id);
                if ($this->Subjects->delete($subjects)) {
                    $this->Flash->success('Subject deleted successfully');
                } else {
                    $this->Flash->error('Subject cannot be deleted');
                }
                return $this->redirect(array('controller' => 'Subjects', 'action' => 'index'));
            }
        } else {
            $this->Flash->error(__('Token miss match'));
            return $this->redirect(['controller' => 'Subjects', 'action' => 'index']);
        }
    }


}
