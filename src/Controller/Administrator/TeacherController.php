<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Administrator;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Schema\Table;

/**
 * CakePHP TeacherController
 * @author Amility
 */
class TeacherController extends AppController {

    public function index() {
	$teacherTable = TableRegistry::get('Teacher');
	$teacher = $teacherTable->find('all')->order(['id' => 'desc']);
	$this->set(compact('teacher'));
	
    }
    
    public function detail($id){
	$teacherTable = TableRegistry::get('Teacher');
	$teacherdetail = $teacherTable->find('all')->where(['id'=>$id])->toArray();
	
	$this->set(compact('teacherdetail'));
    }

}
