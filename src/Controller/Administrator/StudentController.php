<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Administrator;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * CakePHP StudentAndTeacherController
 */
class StudentController extends AppController {

 
    public function index(){
	$studentTable = TableRegistry::get('Student');
	$student = $studentTable->find('all')->order(['id' => 'desc']);
	$this->set(compact('student'));
    }
    
     public function detail($id){
	$studentTable = TableRegistry::get('Student');
	$studentdetail = $studentTable->find('all')->where(['id'=>$id])->toArray();
	$this->set(compact('studentdetail'));
    }

}
