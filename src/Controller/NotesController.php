<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Schema\Table;
use Cake\Core\Configure;

class NotesController extends AppController {

    public $Notes;
    public $Subjects;
    
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Notes = TableRegistry::get('Notes');
        $this->Subjects = TableRegistry::get('Subjects');
    }
    
    
    public function index() {
        $subjects = $this->Subjects->find('list')->select(['sub_id', 'title'])->toArray();        
        $class = Configure::read('Class');
        $chapters = Configure::read('Chapters');
        $this->set(compact(array('subjects','class','chapters')));
        $conditions = array();       
        if (!empty($this->request->query('class')) && !empty($this->request->query('sub_id')) && $this->request->query('chapter_id')) {
            $conditions = ['class' => $this->request->query('class'),'sub_id' => $this->request->query['sub_id'],'chapter' => $this->request->query['chapter_id']];
        }
        //print_r($conditions);exit();
        $notes = $this->paginate($this->Notes->find('all', ['order' => ['Notes.id' => 'DESC']])->where($conditions))->first();        
        $this->set(compact('notes'));
    }

}
