<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Schema\Table;
/**
 * CakePHP TestimonialController
 * @author Amility
 */
class TestimonialController extends AppController {

    public function index() {
	$testimonialTable = TableRegistry::get('Testimonial');
	$testimonial = $testimonialTable->find('all');
	$this->set(compact('testimonial'));	
	
    }

}
